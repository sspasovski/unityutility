﻿using UnityEngine;

namespace Me.StefS.UnityUtility{
public static class Vector3Debugging{
		public static void Show(this Vector3 position){
			GameObject gameObject = GameObject.CreatePrimitive(PrimitiveType.Sphere);
			gameObject.transform.position = position;
		}
	}
}
