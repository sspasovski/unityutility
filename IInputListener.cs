﻿using UnityEngine;

namespace Me.StefS.UnityUtility
{
	public interface IInputListener {
		void OnSwipeLeft(InputController controller);
		void OnSwipeRight(InputController controller);
		//Returns true if it was consumed
		bool OnTap (InputController controller, Collider colliderHit);
		void OnHold(InputController controller);
		void OnHoldEnded(InputController controller);
		void OnSwipeDown(InputController controller);
		void OnSwipeUp (InputController controller);
	}
}