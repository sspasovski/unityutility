﻿using UnityEngine;
using Me.StefS.UnityUtility;
using System.Net.Security;
using System.Collections;
using Me.StefS.Utility;

public abstract class PopupBase : MonoBehaviour, IInputListener {
	[SerializeField]
	private Popup popupPrefab;

	[SerializeField]
	private GameObject contentPrefab;

	protected GameObject Content{ get; private set; }
	private bool isClickInProgress;

	protected Popup Popup{ get; private set;}

	private void Start(){
		this.isClickInProgress = false;
		this.Popup = Instantiate<Popup>(popupPrefab);
		this.Popup.transform.parent = this.transform;
		this.Popup.transform.localPosition = Vector3.zero;
		this.Popup.transform.localRotation = Quaternion.identity;
		this.Content = Instantiate (contentPrefab);
		Content.transform.parent = this.Popup.ContentPlaceholder.transform;
		Content.transform.localPosition = Vector3.zero;
		Content.transform.localRotation = Quaternion.identity;
		this.OnStart ();
	}

	protected abstract void OnStart();
	protected abstract void OnYesClicked(InputController controller);
	protected abstract void OnNoClicked(InputController controller);
	protected abstract bool OnOtherTap (Collider cholliderHit, InputController controller);

	#region IInputListener implementation

	public void OnSwipeLeft (InputController controller)
	{
	}

	public void OnSwipeRight (InputController controller)
	{
	}

	public bool OnTap (InputController controller, Collider colliderHit)
	{
		if(this.isClickInProgress){
			return true;
		}

		if(this.Popup.YesCollider.Equals (colliderHit)){
			this.StartCoroutine (this.ClickYesCorutine (controller));
			return true;
		}

		if(this.Popup.NoCollider.Equals (colliderHit)){
			this.StartCoroutine (this.ClickNoCorutine (controller));
			return true;
		}

		return this.OnOtherTap (colliderHit, controller);
	}

	public void OnHold (InputController controller)
	{
	}

	public void OnHoldEnded (InputController controller)
	{
	}

	public void OnSwipeDown (InputController controller)
	{
	}

	public void OnSwipeUp (InputController controller)
	{
	}

	#endregion

	#region corutines
	private IEnumerator ClickYesCorutine(InputController controller){
		this.isClickInProgress = true;
		yield return Click (this.Popup.YesCollider.transform);
		this.OnYesClicked (controller);
		this.isClickInProgress = false;
	}

	private IEnumerator ClickNoCorutine(InputController controller){
		this.isClickInProgress = true;
		yield return Click (this.Popup.NoCollider.transform);
		this.OnNoClicked (controller);
		this.isClickInProgress = false;
	}


	protected static IEnumerator Click(Transform transform){
		Vector3 originalPosition = transform.localPosition;
		Vector3 clickedPosition = originalPosition + 0.5f.Z ();
		while(!transform.localPosition.IsDistanceBelow (clickedPosition, 0.1f)){
			transform.localPosition = 
				Vector3.Lerp (
					transform.localPosition,
					clickedPosition,
					Time.deltaTime * 10);
			yield return null;
		}

		while(!transform.localPosition.IsDistanceBelow (originalPosition, 0.1f)){
			transform.localPosition = 
				Vector3.Lerp (
					transform.localPosition,
					originalPosition,
					Time.deltaTime * 10);

			yield return null;
		}
	}

	protected void ClosePopup (InputController controller)
	{
		controller.Unsubscribe (this);
		controller.RevokeExclusivePermissions ();
		DestroyImmediate (this.gameObject);
	}
	#endregion
}
