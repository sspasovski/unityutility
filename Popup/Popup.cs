﻿using UnityEngine;

namespace Me.StefS.UnityUtility{
	public class Popup : MonoBehaviour {
		[SerializeField]
		private GameObject contentPlaceholder;

		[SerializeField]
		private Collider yesCollider;

		[SerializeField]
		private Collider noCollider;

		public Collider YesCollider {
			get {
				return yesCollider;
			}
		}

		public Collider NoCollider {
			get {
				return noCollider;
			}
		}

		public GameObject ContentPlaceholder {
			get {
				return contentPlaceholder;
			}
		}
	}
}