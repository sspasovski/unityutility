﻿using UnityEngine;

public class AlternatingEnable : MonoBehaviour {
	[SerializeField]
	private GameObject firstObject;

	[SerializeField]
	private GameObject secondObject;

	[SerializeField]
	private float alternatingInterval;


	private bool isFirstActive;
	private float timeSinseFirstBecomeActive;

	// Use this for initialization
	void Start () {
		this.SetFirstActive (true);
		this.timeSinseFirstBecomeActive = 0f;
	}
	
	// Update is called once per frame
	void Update () {
		this.timeSinseFirstBecomeActive += Time.deltaTime;
		if(isFirstActive && timeSinseFirstBecomeActive > alternatingInterval){
			this.SetFirstActive (false);
		}else if(!isFirstActive && timeSinseFirstBecomeActive > 2 * alternatingInterval){
			this.SetFirstActive (true);
		}
	}

	public void SetFirstActive(bool isFirstActive){
		if(isFirstActive){
			this.timeSinseFirstBecomeActive = 0;
		}

		this.isFirstActive = isFirstActive;
		this.firstObject.SetActive (this.isFirstActive);
		this.secondObject.SetActive (!this.isFirstActive);
	}
}
