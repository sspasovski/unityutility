﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Me.StefS.UnityUtility.MVC {
    public abstract class Controller : MonoBehaviour {
        protected abstract SceneType GeteSceneForWhichThisControllerIs();
        protected abstract void InitializeModel(Model model);
        protected abstract Model DefaultObjectWhenNoModelIsPresent();
        protected abstract IEnumerator OnStart();

        private IEnumerator Start() {
            Scene scene = SceneManager.GetActiveScene();
            /*if (!scene.Equals (SceneManager.GetSceneByName (this.GeteSceneForWhichThisControllerIs ().GetName ())))
            {
                throw new System.ArgumentOutOfRangeException ("The scene should be the one this controller is for");
            }*/

            GameObject containerGameObject = GameObject.Find(ModelContainer.ModelContainerName);
            Model model;
            if (containerGameObject == null) {
                model = this.DefaultObjectWhenNoModelIsPresent();
            }
            else {
                model = containerGameObject.GetComponent<ModelContainer>().Model;
                Destroy(containerGameObject);
            }

            model.PersistantDataPath = Application.persistentDataPath;
            yield return model.Initialize();
            this.InitializeModel(model);
            yield return this.OnStart();
        }
    }
}