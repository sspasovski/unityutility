﻿using UnityEngine;
using Me.StefS.Utility;

namespace Me.StefS.UnityUtility.MVC{
	public class ModelContainer : MonoBehaviour {
		public static string ModelContainerName = "MVC_Model";

		public Model Model{
			get;
			private set;
		}

		public static void CreateModel(Model model){
			GameObject modelObject = new GameObject (ModelContainer.ModelContainerName, typeof(ModelContainer));
			ModelContainer container = modelObject.GetComponent<ModelContainer> ();
			container.Model = model;
			DontDestroyOnLoad (modelObject);
		}
	}
}
