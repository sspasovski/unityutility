﻿using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Me.StefS.UnityUtility.MVC
{
	public abstract class Model
	{
		private string persistantDataPath;
		private const string extension = ".json";

		protected BinaryFormatter binaryFormatter = new BinaryFormatter ();

		public FileStream SaveDat {
			get {
				FileStream saveDat = new FileStream (this.ModelPersistedInFile, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
				return saveDat;
			}
		}

		public FileStream ReadDat {
			get {
				FileStream readDat = new FileStream (this.ModelPersistedInFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read);
				return readDat;
			}
		}

		public string PersistantDataPath {
			get {
				return persistantDataPath;
			}
			set {
				persistantDataPath = value;
				this.ModelPersistedInFile = persistantDataPath
				+ Path.DirectorySeparatorChar
				+ this.GetType ().Name + extension;
			}
		}

		public string ModelPersistedInFile {
			get;
			private set;
		}

		public abstract IEnumerator Initialize ();
	}
}	