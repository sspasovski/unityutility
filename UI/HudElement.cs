﻿using UnityEngine;
namespace Me.StefS.UnityUtility.UI{
	public class HudElement : MonoBehaviour {
		[SerializeField]
		private Anchor anchor;

		public Anchor Anchor {
			get {
				return anchor;
			}
			set {
				anchor = value;
			}
		}
	}
}