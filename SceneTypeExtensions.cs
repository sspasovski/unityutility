﻿using System;

namespace Me.StefS.UnityUtility.MVC
{
	public static class SceneExtensions{
		private static readonly Type sceneType = typeof(SceneType);
		public static string GetName(this SceneType type){
			//TODO put all this in one map, and execute it once
			var memInfo = sceneType.GetMember(type.ToString ());
			var attributes = memInfo[0].GetCustomAttributes(typeof(NameAttribute), false);
			for(int attributeIndex = 0; attributeIndex < attributes.Length; attributeIndex++){
				NameAttribute nameAttribute = attributes [attributeIndex] as NameAttribute;
				if(nameAttribute != null){
					return nameAttribute.Name;
				}
			}

			throw new IndexOutOfRangeException ("type");
		}
	}

	class NameAttribute : Attribute{
		public string Name{ get; private set;}
		public NameAttribute(string name){
			this.Name = name;
		}
	}
}

