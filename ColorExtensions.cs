﻿using UnityEngine;


namespace Me.StefS.UnityUtility
{
	public static class ColorExtensions
	{
		public static float DifferenceWith (this Color firstColor, Color secondColor)
		{
			return Mathf.Abs (firstColor.r - secondColor.r)
			+ Mathf.Abs (firstColor.g - secondColor.g)
			+ Mathf.Abs (firstColor.b - secondColor.b)
			+ Mathf.Abs (firstColor.a - secondColor.a);
		}

		public static string ToHexString(this Color color){
			return Mathf.RoundToInt (color.r * 256).ToString ("X2")
				+ Mathf.RoundToInt (color.g * 256).ToString ("X2")
				+ Mathf.RoundToInt (color.b * 256).ToString ("X2");
		}
	}
}
