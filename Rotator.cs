﻿using UnityEngine;
using System;
using Me.StefS.Utility;

namespace Me.StefS.UnityUtility{
	public class Rotator : MonoBehaviour {
		[SerializeField]
		private Axis aroundAxis;

		[SerializeField]
		private float maxAngle;

		[SerializeField]
		private float stepAngle;

		[SerializeField]
		private bool continues;

		[SerializeField]
		private Signum direction;
		private float currentAngle;
		private Vector3 rotationAxis;
		private Quaternion startRotation;

		private void Awake(){
			switch(aroundAxis){
			case Axis.X:
				rotationAxis = new Vector3 (1, 0, 0);
				break;
			case Axis.Y:
				rotationAxis = new Vector3 (0, 1, 0);
				break;
			case Axis.Z:
				rotationAxis = new Vector3 (0,0,1);
				break;
				default:
				throw new System.InvalidOperationException("there should always be case for the axis");
			}
			this.startRotation = this.transform.localRotation;
			currentAngle = 0;
		}

		private void Update(){
			currentAngle = currentAngle + ((int)this.direction * this.stepAngle * Time.deltaTime);
			if(!this.continues){
				if(currentAngle >= maxAngle){
					this.direction = Signum.Minus;
				}else if(currentAngle <= - maxAngle){
					this.direction = Signum.Plus;
				}
			}

			this.transform.localRotation = this.startRotation * Quaternion.AngleAxis (currentAngle, rotationAxis);
		}
	}
}
