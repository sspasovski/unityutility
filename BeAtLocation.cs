﻿using UnityEngine;
using Me.StefS.Utility;

namespace Me.StefS.UnityUtility{
	public class BeAtLocation : MonoBehaviour {
		private bool IsInitialized;
		private LocationProvider LocationProvider;

		public void Initialize(LocationProvider locationProvider){
			this.LocationProvider = locationProvider;
			this.IsInitialized = true;
		}

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
			if(!IsInitialized){
				return;
			}

			this.transform.localPosition = LocationProvider.Location;
		}
	}
}
