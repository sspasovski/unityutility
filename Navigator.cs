﻿using UnityEngine.SceneManagement;
using Me.StefS.UnityUtility.MVC;

namespace Me.StefS.UnityUtility{
	public static class Navigator  {
		public static void GoToScene(SceneType type, Model model){
			ModelContainer.CreateModel (model);
			SceneManager.LoadScene (type.GetName ());
		}
	}
}